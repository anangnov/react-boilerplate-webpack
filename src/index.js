import React from 'react';
import ReactDOM from 'react-dom';
import './assets/css/main.css';
import 'bootstrap/dist/css/bootstrap.min.css';
// import App from './components/App';
import App from './routes';

ReactDOM.render(<App />, document.getElementById('root'));
