import React from 'react';
import img1 from '../assets/images/bc-1.jpg';
import img2 from '../assets/images/bc-2.jpg';

const App = () => {
  return (
    <div>
      <nav className="navbar navbar-light navbar-inner">
        <span className="navbar-brand mb-0 h1">Navbar</span>
      </nav>
      <div className="container mt-4 mb-4">
        <div className="row">
          <div className="col-md-6">
            <div className="card card-custom">
              <img src={img1} className="card-img-top" alt="Image" />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">
                  Some quick example text to build on the card title and make up the bulk of the card's content.
                </p>
                <a href="#" class="btn btn-primary">
                  Go somewhere
                </a>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div className="card card-custom">
              <img src={img2} className="card-img-top" alt="Image" />
              <div className="card-body">
                <h5 className="card-title">Card title</h5>
                <p className="card-text">
                  Some quick example text to build on the card title and make up the bulk of the card's content.
                </p>
                <a href="#" class="btn btn-primary">
                  Go somewhere
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
