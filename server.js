const express = require('express');
const path = require('path');
const port = 8084;
const app = express();

app.use(express.static(__dirname + '/build'));
let root = path.resolve(__dirname, 'build', 'index.html');

app.get('*', function (request, response) {
  response.sendFile(root);
});

app.use(express.static('/'));
app.listen(port);
console.log('Server started on port ' + port);
